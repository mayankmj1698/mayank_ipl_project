// Total no of matches played per year

function matchesPlayedPerYear(matches)
{
    const year =matches.map(match => match.season)
    const matchplay = year.reduce((matchplay,year) => {

        if(matchplay[year]) {
            
            matchplay[year]+=1
        }
        else{

            matchplay[year]=1
        }
        
        return matchplay
    }, {} )

    return matchplay
}


// number of matches won each team per year

function matchesWonByEachTeamPerYear(matches){

    const matchwon= matches.reduce((teamsWon, match) => {

        const season = match['season'];
        const winner = match['winner'];

        if(season in teamsWon) {

            if(winner in teamsWon[season]) {

                teamsWon[season][winner] +=1;
            
            } else {
                teamsWon[season][winner] =1;
            }

        } else {
            teamsWon[season] = {};
            teamsWon[season][winner] =1;
        }

        return teamsWon;
        
    }, {})
    return matchwon

}


//Extra run conduct each team in year 2016

function extraRunsConductPerTeamIn2016(matches,deliveries){
    const matchid=matches.filter(match=>{              
        if(match.season=='2016'){
            return match.id;
        }
    }).map(match=>parseInt(match.id));

    const deliver=deliveries.filter(delivery=>{        
        if(matchid.includes(parseInt(delivery.match_id))){
            return delivery;
        }
    })

    let extraRun=deliver.reduce(function(run,ball){        
        if(run[ball.bowling_team]){
            run[ball.bowling_team]+= parseInt(ball.extra_runs);
            
        }
        else{
            run[ball.bowling_team]= parseInt(ball.extra_runs);
        }
        return run;

    },{})
    return extraRun;
}


//Top 10 economical bowlers in the year 2015

function topEconomicalBowlersIn2015 (matches,deliveries){
    var overs={};
    var runs={};
    for(let match of matches){
for(let delivery of deliveries)
{    
    if(match.id===delivery.match_id && match.season=='2015'){
      if(runs[delivery.bowler])
        {
        runs[delivery.bowler]+=parseInt(delivery.total_runs);
        if(delivery.ball=='1'){
            overs[delivery.bowler]+=1;
        }
    }
     else{
        runs[delivery.bowler]=parseInt(delivery.total_runs);
        overs[delivery.bowler]=1;
         }
     }
}
    }
var result={};
for(let i in runs){
result[i]=parseFloat((runs[i]/overs[i]).toFixed(2));
}
    let arrEco=Object.values(result);
arrEco.sort(function(a,b){ return a-b });       
 var last={};
 for(let i=0;i<10;i++){
 for(let j in result){
     if(result[j]==arrEco[i]){
     last[j]=arrEco[i];
 }
}
}
return last;
}


module.exports = {
    matchesPlayedPerYear,
    matchesWonByEachTeamPerYear,
    extraRunsConductPerTeamIn2016,
    topEconomicalBowlersIn2015 ,
    
    
}

