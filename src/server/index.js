const fs = require("fs");
const path = require('path');
const output_File_Path = "../public/output/";


const matchFilePath = '../data/matches.json'
const deliveryFilePath = '../data/deliveries.json'
const ipl = require("./ipl.js")

    const matches = JSON.parse(fs.readFileSync(path.resolve(__dirname, matchFilePath)))
    const deliveries = JSON.parse(fs.readFileSync(path.resolve(__dirname, deliveryFilePath)))


    function writeJsonToOutput(data, outputPath){
      fs.writeFile(path.resolve( `../public/output/${outputPath}.json`), JSON.stringify(data), (err) => {
          if (err) throw err;
          else console.log(`successfully done! ${outputPath}`)
      });
  }
  
//Match Played per year
const MatchesPlayedPerYear = ipl.matchesPlayedPerYear(matches);
writeJsonToOutput(MatchesPlayedPerYear,`MatchesPlayedPerYear`);
//console.log(result);

//Matches won by each team
const MatchesWonByEachTeamPerYear = ipl.matchesWonByEachTeamPerYear(matches);
writeJsonToOutput(MatchesWonByEachTeamPerYear,`MatchesWonByEachTeamPerYear`);
//console.log(result2);


//Extra run conduct per team in year 2016
const ExtraRunsConduct=ipl.extraRunsConductPerTeamIn2016(matches,deliveries)
writeJsonToOutput(ExtraRunsConduct,`ExtraRunsConduct`);
//console.log(result3)

//Top 10 economical bowlers
const TopEconomicalBowlersIn2015=ipl.topEconomicalBowlersIn2015 (matches,deliveries)
writeJsonToOutput(TopEconomicalBowlersIn2015,`TopEconomicalBowlersIn2015`);
//console.log(result4)

