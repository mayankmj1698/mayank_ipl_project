const http = require('http')
const path = require('path')
const fs = require('fs')

const server = http.createServer((request, response) => {

    const requestURL = request.url;
    const requestUrlFileExt = request.url.split(".").splice(-1)[0];
    const client_File_Path = path.join(__dirname, "../client/");
    const output_File_Path = path.join(__dirname, "../public/output/");    

    switch(requestUrlFileExt){

        case '/':
            const html_File_Path = path.join(__dirname, "../client/index.html")
            fs.readFile(html_File_Path, 'utf-8', (err, htmlData) => {
                if(err) {
                    response.writeHead(404);
                } else {
                    response.writeHead(200, {'Content-Type': "text/html" });
                    response.end(htmlData);
                }
            })

            break;
            
        case 'js':

            const js_File_Path = path.join(__dirname, "../client/plot.js")

            console.log(js_File_Path)

            fs.readFile(js_File_Path, "utf-8", (err, jsData) => {
                if (err) {

                    response.writeHead(404);
                    console.log(err)

                } else {

                    response.writeHead(200, { 'Content-Type': "application/javascript" });
                    response.end(jsData);

                }
            });

            break;

        case 'json':

            const json_File_Path = path.join(output_File_Path, requestURL);

            fs.readFile(json_File_Path, "utf-8", (err, jsonData) => {

                if (err) {

                    response.writeHead(404);

                } else {

                    response.writeHead(200, { 'Content-Type': "application/json" });
                    response.end(jsonData);

                }
            });
            break;
    }
});

server.listen(3000, () => console.log('web server running on port 3000'))
