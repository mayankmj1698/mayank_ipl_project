const fs = require('fs')
const csv = require('csvtojson')
const path = require('path')

const dirname = require.main.path
const matchesFilePath = "../src/data/matches.csv";
const deliveryFilePath = "../src/data/deliveries.csv";

const modifyFilePath = filePath => filePath.replace('.csv', '.json')

async function csvtojsonConverter(csvFileName){
    const data = await csv().fromFile(path.join(dirname, csvFileName))
    
    const outFilePath = modifyFilePath(csvFileName)
    console.log({outFilePath})
    fs.writeFileSync(path.resolve(dirname, outFilePath), JSON.stringify(data))
}


csvtojsonConverter(matchesFilePath)
csvtojsonConverter(deliveryFilePath)
